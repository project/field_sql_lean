<?php

/**
 * @file
 * Implementation of the lean field storage API.
 *
 * Here is the sql which get the field data table: "mysql> select fsl.entity as
 * enitity_type, n.type as bundle, f.*  from field_revision_field_program_name f
 * join fsl_entity fsl on fsl.eid = f.entity inner join node n on n.nid =
 * f.entity_id and n.vid = f.revision_id;"
 */

/**
 * Implements hook_field_storage_info_alter().
 *
 * We override field_sql_storage's module pointer to point to ourself
 * (field_sql_lean) so we can override all relevant functions.
 */
function field_sql_lean_field_storage_info_alter(&$info) {
  $field_sql_lean_on = variable_get('field_sql_lean_on', FALSE);
  if ($field_sql_lean_on) {
    $info['field_sql_storage']['module'] = 'field_sql_lean';
  }
}

/**
 * Implements hook_field_storage_info().
 */
function field_sql_lean_field_storage_info() {
  return array(
    'field_sql_storage' => array(
      'label' => t('Default SQL storage'),
      'description' => t('Stores fields in the local SQL database, using per-field tables.'),
    ),
  );
}

/**
 * Implements hook_help().
 */
function field_sql_lean_help($path, $arg) {
  switch ($path) {
    case 'admin/help#field_sql_lean':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The Field SQL lean storage module stores minimal field data in the database. It rewrites default field storage module.') . '</p>';
      return $output;
  }
}

/**
 * Implements hook_menu().
 */
function field_sql_lean_menu() {
  $items = array();

  $items['admin/config/system/field_sql_lean/field_sql_lean_batch_alter'] = array(
    'title' => 'Field SQL Lean Batch Alter',
    'description' => 'Alter database tables for entities',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('field_sql_lean_batch_alter_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
    'weight' => 1,
    'file' => 'field_sql_lean.admin.inc',
  );

  return $items;
}

/**
 * Callback function after batch is finished().
 */
function field_sql_lean_batch_finish() {
  variable_set('field_sql_lean_on', TRUE);
  cache_clear_all();
}

/**
 * Callback function after batch is finished().
 */
function field_sql_lean_batch_finish_reverse() {
  variable_set('field_sql_lean_on', FALSE);
  cache_clear_all();
}

/**
 * Implements hook_field_storage_create_field().
 */
function field_sql_lean_field_storage_create_field($field) {
  $schema = _field_sql_lean_schema($field);
  foreach ($schema as $name => $table) {
    db_create_table($name, $table);
  }
  drupal_get_schema(NULL, TRUE);
}

/**
 * Implements hook_field_update_forbid().
 *
 * Forbid any field update that changes column definitions if there is
 * any data.
 */
function field_sql_lean_field_update_forbid($field, $prior_field, $has_data) {
  if ($has_data && $field['columns'] != $prior_field['columns']) {
    throw new FieldUpdateForbiddenException("field_sql_lean cannot change the schema for an existing field with data.");
  }
}

/**
 * Implements hook_field_storage_update_field().
 */
function field_sql_lean_field_storage_update_field($field, $prior_field, $has_data) {
  if (!$has_data) {
    // There is no data. Re-create the tables completely.

    if (Database::getConnection()->supportsTransactionalDDL()) {
      // If the database supports transactional DDL, we can go ahead and rely
      // on it. If not, we will have to rollback manually if something fails.
      $transaction = db_transaction();
    }

    try {
      $prior_schema = _field_sql_lean_schema($prior_field);
      foreach ($prior_schema as $name => $table) {
        db_drop_table($name, $table);
      }
      $schema = _field_sql_lean_schema($field);
      foreach ($schema as $name => $table) {
        db_create_table($name, $table);
      }
    }
    catch (Exception $e) {
      if (Database::getConnection()->supportsTransactionalDDL()) {
        $transaction->rollback();
      }
      else {
        // Recreate tables.
        $prior_schema = _field_sql_lean_schema($prior_field);
        foreach ($prior_schema as $name => $table) {
          if (!db_table_exists($name)) {
            db_create_table($name, $table);
          }
        }
      }
      throw $e;
    }
  }
  else {
    // There is data, so there are no column changes. Drop all the
    // prior indexes and create all the new ones, except for all the
    // priors that exist unchanged.
    $revision_table = _field_sql_storage_revision_tablename($prior_field);
    foreach ($prior_field['indexes'] as $name => $columns) {
      if (!isset($field['indexes'][$name]) || $columns != $field['indexes'][$name]) {
        $real_name = _field_sql_storage_indexname($field['field_name'], $name);
        db_drop_index($revision_table, $real_name);
      }
    }
    $revision_table = _field_sql_storage_revision_tablename($field);
    foreach ($field['indexes'] as $name => $columns) {
      if (!isset($prior_field['indexes'][$name]) || $columns != $prior_field['indexes'][$name]) {
        $real_name = _field_sql_storage_indexname($field['field_name'], $name);
        $real_columns = array();
        foreach ($columns as $column_name) {
          $real_columns[] = _field_sql_storage_columnname($field['field_name'], $column_name);
        }
        db_add_index($revision_table, $real_name, $real_columns);
      }
    }
  }
  drupal_get_schema(NULL, TRUE);
}

/**
 * Implements hook_field_storage_delete_field().
 */
function field_sql_lean_field_storage_delete_field($field) {
  $table = _field_sql_storage_revision_tablename($field);
  db_drop_table($table);
  drupal_get_schema(NULL, TRUE);
}

/**
 * Get static internal entity ID array.
 */
function field_sql_lean_entity_id($reset = FALSE) {
  static $field_sql_lean_entity_id;
  if (empty($field_sql_lean_entity_id) || $reset) {
    $field_sql_lean_entity_id = array();
    $query = db_select('fsl_entity', 'e')
      ->fields('e');
    $results = $query->execute();
    foreach ($results as $row) {
      $field_sql_lean_entity_id[$row->entity] = $row->eid;
    }
  }
  return $field_sql_lean_entity_id;
}

/**
 * Get revision id for the field.
 */
function _field_sql_lean_get_revision_id($entity_type, $field) {
  static $entity_ids, $entity_vids;
  if ($entity_ids !== $field) {
    $entity_ids = $field;
    $entity_vids = array();
    $einfo = entity_get_info($entity_type);
    $rfield = ($entity_type == 'node') ? 'vid' : 'revision_id';
    $mfield = ($entity_type == 'node') ? 'nid' : 'entity_id';
    $query = db_select($einfo['base table'], 'e')
      ->fields('e', (array($mfield, $rfield)))
      ->condition($mfield, $entity_ids, 'IN');
    $results = $query->execute();
    $tgvid = array();
    foreach ($results as $row) {
      $tgvid[$row->$mfield] = $row->$rfield;
    }
    foreach ($entity_ids as $key => $entity_id) {
      $entity_vids[$key] = $tgvid[$entity_id];
    }

  }
  return $entity_vids;
}

/**
 * Get a list of entity using revision.
 */
function field_sql_lean_get_revision_entity_type() {
  $entities = entity_get_info();
  $return = array();
  foreach ($entities as $name => $entity) {
    if (!empty($entity['entity keys']['revision'])) {
      $return[] = $name;
    }
  }
  return $return;
}

/**
 * Implements hook_field_storage_load().
 */
function field_sql_lean_field_storage_load($entity_type, $entities, $age, $fields, $options) {
  $load_current = $age == FIELD_LOAD_CURRENT;

  $revision_entity = field_sql_lean_get_revision_entity_type();
  $load_current_vid = FALSE;

  // If loading fields of revisionable entity, get the current revision.
  if ($load_current && in_array($entity_type, $revision_entity)) {
    $load_current_vid = TRUE;
    $load_current = FALSE;
  }
  $eid = field_sql_lean_entity_id();

  foreach ($fields as $field_id => $ids) {
    if ($load_current_vid) {
      $ids = _field_sql_lean_get_revision_id($entity_type, $ids);
    }
    // By the time this hook runs, the relevant field definitions have been
    // populated and cached in FieldInfo, so calling field_info_field_by_id()
    // on each field individually is more efficient than loading all fields in
    // memory upfront with field_info_field_by_ids().
    $field = field_info_field_by_id($field_id);
    $field_name = $field['field_name'];
    $table = _field_sql_storage_revision_tablename($field);

    $query = db_select($table, 't')
      ->fields('t')
      ->condition('entity', $eid[$entity_type])
      ->condition($load_current ? 'entity_id' : 'revision_id', $ids, 'IN')
      ->condition('language', field_available_languages($entity_type, $field), 'IN')
      ->orderBy('delta');

    $results = $query->execute();

    $delta_count = array();
    foreach ($results as $row) {
      if (!isset($delta_count[$row->entity_id][$row->language])) {
        $delta_count[$row->entity_id][$row->language] = 0;
      }

      if ($field['cardinality'] == FIELD_CARDINALITY_UNLIMITED || $delta_count[$row->entity_id][$row->language] < $field['cardinality']) {
        $item = array();
        // For each column declared by the field, populate the item
        // from the prefixed database column.
        foreach ($field['columns'] as $column => $attributes) {
          $column_name = _field_sql_storage_columnname($field_name, $column);
          $item[$column] = $row->$column_name;
        }

        // Add the item to the field values for the entity.
        $entities[$row->entity_id]->{$field_name}[$row->language][] = $item;
        $delta_count[$row->entity_id][$row->language]++;
      }
    }
  }
}

/**
 * Implements hook_field_storage_write().
 */
function field_sql_lean_field_storage_write($entity_type, $entity, $op, $fields) {
  $eid = field_sql_lean_entity_id();
  $eid_entity = array_flip($eid);

  list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);
  if (!isset($vid)) {
    $vid = $id;
  }

  foreach ($fields as $field_id) {
    $field = field_info_field_by_id($field_id);
    $field_name = $field['field_name'];
    $revision_name = _field_sql_storage_revision_tablename($field);

    $all_languages = field_available_languages($entity_type, $field);
    $field_languages = array_intersect($all_languages, array_keys((array) $entity->$field_name));

    // Delete and insert, rather than update, in case a value was added.
    if ($op == FIELD_STORAGE_UPDATE) {
      // Delete languages present in the incoming $entity->$field_name.
      // Delete all languages if $entity->$field_name is empty.
      $languages = !empty($entity->$field_name) ? $field_languages : $all_languages;
      if ($languages) {
        db_delete($revision_name)
          ->condition('entity', $eid[$entity_type])
          ->condition('entity_id', $id)
          ->condition('revision_id', $vid)
          ->condition('language', $languages, 'IN')
          ->execute();
      }
    }

    // Prepare the multi-insert query.
    $do_insert = FALSE;
    $columns = array('entity', 'entity_id', 'revision_id', 'delta', 'language');
    foreach ($field['columns'] as $column => $attributes) {
      $columns[] = _field_sql_storage_columnname($field_name, $column);
    }
    $revision_query = db_insert($revision_name)->fields($columns);

    foreach ($field_languages as $langcode) {
      $items = (array) $entity->{$field_name}[$langcode];
      $delta_count = 0;
      foreach ($items as $delta => $item) {
        // We now know we have someting to insert.
        $do_insert = TRUE;
        $record = array(
          'entity' => $eid[$entity_type],
          'entity_id' => $id,
          'revision_id' => $vid,
          'delta' => $delta,
          'language' => $langcode,
        );
        foreach ($field['columns'] as $column => $attributes) {
          $record[_field_sql_storage_columnname($field_name, $column)] = isset($item[$column]) ? $item[$column] : NULL;
        }
        $revision_query->values($record);

        if ($field['cardinality'] != FIELD_CARDINALITY_UNLIMITED && ++$delta_count == $field['cardinality']) {
          break;
        }
      }
    }

    // Execute the query if we have values to insert.
    if ($do_insert) {
      $revision_query->execute();
    }
  }
}

/**
 * Implements hook_field_storage_delete().
 *
 * This function deletes data for all fields for an entity from the database.
 */
function field_sql_lean_field_storage_delete($entity_type, $entity, $fields) {
  list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);

  foreach (field_info_instances($entity_type, $bundle) as $instance) {
    if (isset($fields[$instance['field_id']])) {
      $field = field_info_field_by_id($instance['field_id']);
      field_sql_lean_field_storage_purge($entity_type, $entity, $field, $instance);
    }
  }
}

/**
 * Implements hook_field_storage_purge().
 *
 * This function deletes data from the database for a single field on
 * an entity.
 */
function field_sql_lean_field_storage_purge($entity_type, $entity, $field, $instance) {
  $eid = field_sql_lean_entity_id();
  list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);

  $table_name = _field_sql_storage_revision_tablename($field);
  db_delete($table_name)
    ->condition('entity', $eid[$entity_type])
    ->condition('entity_id', $id)
    ->execute();
}

/**
 * Implements hook_field_storage_query().
 *
 * ToDO: need more code here.
 */
function field_sql_lean_field_storage_query(EntityFieldQuery $query) {
  if ($query->age == FIELD_LOAD_CURRENT) {
    $id_key = 'entity_id';
  }
  else {
    $id_key = 'revision_id';
  }
  $table_aliases = array();
  $query_tables = NULL;
  // Add tables for the fields used.
  foreach ($query->fields as $key => $field) {
    $tablename = _field_sql_storage_revision_tablename($field);
    $table_alias = _field_sql_storage_tablealias($tablename, $key, $query);
    $table_aliases[$key] = $table_alias;
    if ($key) {
      if (!isset($query_tables[$table_alias])) {
        $select_query->join($tablename, $table_alias, "$table_alias.entity = $field_base_table.entity AND $table_alias.$id_key = $field_base_table.$id_key");
      }
    }
    else {
      $select_query = db_select($tablename, $table_alias);
      // Store a reference to the list of joined tables.
      $query_tables =& $select_query->getTables();
      // Allow queries internal to the Field API to opt out of the access
      // check, for situations where the query's results should not depend on
      // the access grants for the current user.
      if (!isset($query->tags['DANGEROUS_ACCESS_CHECK_OPT_OUT'])) {
        $select_query->addTag('entity_field_access');
      }
      $select_query->addMetaData('base_table', $tablename);
      $select_query->join('fsl_entity', 'fsle', $table_alias . '.entity = fsle.eid');
      $select_query->fields($table_alias, array('entity_id', 'revision_id'));
      $select_query->addField('fsle', 'entity', 'entity_type');
      $field_base_table = $table_alias;
    }
    if ($field['cardinality'] != 1 || $field['translatable']) {
      $select_query->distinct();
    }
  }

  // Add field conditions. We need a fresh grouping cache.
  drupal_static_reset('_field_sql_storage_query_field_conditions');
  _field_sql_storage_query_field_conditions($query, $select_query, $query->fieldConditions, $table_aliases, '_field_sql_storage_columnname');

  // Add field meta conditions.
  _field_sql_storage_query_field_conditions($query, $select_query, $query->fieldMetaConditions, $table_aliases, '_field_sql_storage_query_columnname');

  if (isset($query->deleted) && $query->deleted) {
    return;
  }

  // Is there a need to sort the query by property?
  $has_property_order = FALSE;
  foreach ($query->order as $order) {
    if ($order['type'] == 'property') {
      $has_property_order = TRUE;
    }
  }

  if ($query->propertyConditions || $has_property_order || !(empty($query->entityConditions['bundle']['value']))) {
    if (empty($query->entityConditions['entity_type']['value'])) {
      throw new EntityFieldQueryException('Property conditions, bundle entity condition and orders must have an entity type defined.');
    }
    $entity_type = $query->entityConditions['entity_type']['value'];
    $entity_base_table = _field_sql_storage_query_join_entity($select_query, $entity_type, $field_base_table);
    $query->entityConditions['entity_type']['operator'] = '=';
    foreach ($query->propertyConditions as $property_condition) {
      $query->addCondition($select_query, "$entity_base_table." . $property_condition['column'], $property_condition);
    }
  }
  foreach ($query->entityConditions as $key => $condition) {
    if ($key === 'bundle') {
      if (isset($query->entityConditions['entity_type'])) {
        $etype = $query->entityConditions['entity_type']['value'];
        if ($etype === 'comment') {
        }
        elseif ($etype === 'taxonomy_term') {
        }
        else {
          $einfo = entity_get_info($etype);
          $select_query->condition("$entity_base_table." . $einfo['entity keys']['bundle'], $condition['value']);
          $select_query->addField("$entity_base_table", $einfo['entity keys']['bundle'], 'bundle');
        }
      }
    }
    elseif ($key === 'entity_type') {
      $select_query->condition('fsle.entity', $condition['value']);
    }
    else {
      $query->addCondition($select_query, "$field_base_table.$key", $condition);
    }
  }
  // Order the query.
  foreach ($query->order as $order) {
    if ($order['type'] == 'entity') {
      $key = $order['specifier'];
      $select_query->orderBy("$field_base_table.$key", $order['direction']);
    }
    elseif ($order['type'] == 'field') {
      $specifier = $order['specifier'];
      $field = $specifier['field'];
      $table_alias = $table_aliases[$specifier['index']];
      $sql_field = "$table_alias." . _field_sql_storage_columnname($field['field_name'], $specifier['column']);
      $select_query->orderBy($sql_field, $order['direction']);
    }
    elseif ($order['type'] == 'property') {
      $select_query->orderBy("$entity_base_table." . $order['specifier'], $order['direction']);
    }
  }
  $result = $query->finishQuery($select_query, $id_key);
  return $result;
}

/**
 * Implements hook_field_storage_delete_revision().
 *
 * This function actually deletes the data from the database.
 */
function field_sql_lean_field_storage_delete_revision($entity_type, $entity, $fields) {
  $eid = field_sql_lean_entity_id();
  list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);

  if (isset($vid)) {
    foreach ($fields as $field_id) {
      $field = field_info_field_by_id($field_id);
      $revision_name = _field_sql_storage_revision_tablename($field);
      db_delete($revision_name)
        ->condition('entity', $eid[$entity_type])
        ->condition('entity_id', $id)
        ->condition('revision_id', $vid)
        ->execute();
    }
  }
}

/**
 * Implements hook_field_storage_delete_instance().
 *
 * This function simply marks for deletion all data associated with the field.
 */
function field_sql_lean_field_storage_delete_instance($instance) {
  $eid = field_sql_lean_entity_id();
  $field = field_info_field($instance['field_name']);
  $revision_name = _field_sql_storage_revision_tablename($field);

  // For now, we only handle this on node entity type.
  $ids = array();
  switch ($instance['entity_type']) {
    case 'node':
      $query = db_select('node', 'n')
        ->fields('n', array('nid'))
        ->condition('type', $instance['bundle']);
      $results = $query->execute();
      foreach ($results as $row) {
        $ids[] = $row->nid;
      }
      break;

    case 'comment':
      $query = db_select('comment', 'c');
      $query->join('node', 'n', 'c.nid = n.nid');
      $query->fields('c', array('cid'))
        ->condition('n.type', substr($instance['bundle'], 13));
      $results = $query->execute();
      foreach ($results as $row) {
        $ids[] = $row->cid;
      }
      break;

    case 'user':
      $query = db_select('users', 'u')
        ->fields('u', array('uid'));
      $results = $query->execute();
      foreach ($results as $row) {
        $ids[] = $row->uid;
      }
      break;

    case 'taxonomy_term':
      $query = db_select('taxonomy_term_data', 't');
      $query->join('taxonomy_vocabulary', 'v', 't.vid = v.vid');
      $query->fields('t', array('tid'))
        ->condition('v.machine_name', $instance['bundle']);
      $results = $query->execute();
      foreach ($results as $row) {
        $ids[] = $row->tid;
      }
      break;

    default:
      $einfo = entity_get_info($instance['entity_type']);
      $entity_id = $einfo['entity keys']['id'];
      $bundle_key = isset($einfo['entity keys']['bundle']) ? $einfo['entity keys']['bundle'] : '';
      if ($bundle_key === '') {
        $bundle_key = isset($einfo['bundle keys']['bundle']) ? $einfo['bundle keys']['bundle'] : '';
      }
      if ($bundle_key === '') {
        watchdog('field_sql_lean', 'When deleting the field data, we found entity bundle key was not set for this entity', array(), WATCHDOG_WARNING);
        return;
      }
      $query = db_select($einfo['base table'], 'u')
        ->fields('u', array($entity_id))
        ->condition($bundle_key, $instance['bundle']);
      $results = $query->execute();
      foreach ($results as $row) {
        $ids[] = $row->$entity_id;
      }
  }

  // Put it in batch in case there are lots of instances.
  // TODO: put it either in queue or batch job if php time out.
  $num = count($ids);
  if ($num > 0) {
    $bsize = variable_get('fsl_batch_size', 10000);
    $bsize = intval($bsize);
    if ($num <= $bsize) {
      db_delete($revision_name)
        ->condition('entity', $eid[$instance['entity_type']])
        ->condition('entity_id', $ids, 'IN')
        ->execute();
    }
    else {
      // We tested with over 50,000 nodes, works well. Need replace this part of
      // code if php time out here.
      db_delete($revision_name)
        ->condition('entity', $eid[$instance['entity_type']])
        ->condition('entity_id', $ids, 'IN')
        ->execute();
    }
  }
}

/**
 * Implements hook_field_attach_rename_bundle().
 */
function field_sql_lean_field_attach_rename_bundle($entity_type, $bundle_old, $bundle_new) {
  // Change bundle will not affect field, so nothing to do here.
}

/**
 * Implements hook_field_storage_purge_field().
 *
 * All field data items and instances have already been purged, so all
 * that is left is to delete the table.
 */
function field_sql_lean_field_storage_purge_field($field) {
  $revision_name = _field_sql_storage_revision_tablename($field);
  db_drop_table($revision_name);
}

/**
 * Implements hook_field_storage_details().
 */
function field_sql_lean_field_storage_details($field) {
  return field_sql_storage_field_storage_details($field);
}

/**
 * Return the database schema for a field.
 *
 * This may contain one or
 * more tables. Each table will contain the columns relevant for the
 * specified field. Leave the $field's 'columns' and 'indexes' keys
 * empty to get only the base schema.
 */
function _field_sql_lean_schema($field) {
  $deleted = $field['deleted'] ? 'deleted ' : '';
  $current = array(
    'description' => "Data storage for {$deleted}field {$field['id']} ({$field['field_name']})",
    'fields' => array(
      'entity_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'The entity id this data is attached to',
      ),
      'revision_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'description' => 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
      ),
      // @todo Consider storing language as integer.
      'language' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The language for this data item.',
      ),
      'delta' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'The sequence number for this data item, used for multi-value fields',
      ),
      'entity' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Entity type ID',
      ),
    ),
    'indexes' => array(
      'entity_type' => array('entity'),
      'entity_id' => array('entity_id'),
      'revision_id' => array('revision_id'),
      'language' => array('language'),
    ),
  );

  $field += array(
    'columns' => array(),
    'indexes' => array(),
    'foreign keys' => array(),
  );

  // Add field columns.
  foreach ($field['columns'] as $column_name => $attributes) {
    $real_name = _field_sql_storage_columnname($field['field_name'], $column_name);
    $current['fields'][$real_name] = $attributes;
  }

  // Add indexes.
  foreach ($field['indexes'] as $index_name => $columns) {
    $real_name = _field_sql_storage_indexname($field['field_name'], $index_name);
    foreach ($columns as $column_name) {
      $current['indexes'][$real_name][] = _field_sql_storage_columnname($field['field_name'], $column_name);
    }
  }

  // Add foreign keys.
  foreach ($field['foreign keys'] as $specifier => $specification) {
    $real_name = _field_sql_storage_indexname($field['field_name'], $specifier);
    $current['foreign keys'][$real_name]['table'] = $specification['table'];
    foreach ($specification['columns'] as $column_name => $referenced) {
      $sql_storage_column = _field_sql_storage_columnname($field['field_name'], $column_name);
      $current['foreign keys'][$real_name]['columns'][$sql_storage_column] = $referenced;
    }
  }

  // Construct the revision table.
  $revision = $current;
  $revision['description'] = "Revision archive storage for {$deleted}field {$field['id']} ({$field['field_name']})";
  $revision['primary key'] = array(
    'entity', 'entity_id',
    'revision_id', 'delta',
    'language',
  );
  $revision['fields']['revision_id']['not null'] = TRUE;
  $revision['fields']['revision_id']['description'] = 'The entity revision id this data is attached to';

  return array(
    _field_sql_storage_revision_tablename($field) => $revision,
  );
}

/**
 * Implements hook_entity_info_alter().
 */
function field_sql_lean_entity_info_alter(&$entity_info) {
  $current = array_keys($entity_info);
  $eid = field_sql_lean_entity_id(TRUE);
  $old = array_keys($eid);
  foreach ($old as $entity) {
    if (in_array($entity, $current)) {
    }
    else {
      db_delete('fsl_entity')
        ->condition('entity', $entity)
        ->execute();
    }
  }

  foreach ($current as $entity) {
    if (in_array($entity, $old)) {
    }
    else {
      db_insert('fsl_entity')
        ->fields(array('entity'), array($entity))
        ->execute();
    }
  }
}
