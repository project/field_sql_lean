<?php

/**
 * @file
 * Forms for field_sql_lean admin screens
 */

/**
 * Form callback for batch deletion of revisions.
 */
function field_sql_lean_batch_alter_form($form, &$form_state) {
  $form = array();

  $field_sql_lean_on = variable_get('field_sql_lean_on', FALSE);
  if ($field_sql_lean_on) {
    $form['description'] = array(
      '#markup' => '<h3>' . t('Database have been lean processed. Backup database if you want to revert it back.') . '</h3>',
      '#weight' => 0,
    );
    $form['field_sql_lean_revert_flag'] = array(
      '#type' => 'checkbox',
      '#title' => 'Reverse the lean process.',
      '#default_value' => FALSE,
      '#weight' => 11,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Lean Alter Reverse'),
      '#submit' => array('field_sql_lean_batch_alter_form_reverse_submit'),
      '#weight' => 22,
    );
  }
  else {
    $form['description'] = array(
      '#markup' => '<h3>' . t('Set to site to maintenance mode and backup database.') . '</h3>',
      '#weight' => 0,
    );
    $form['field_sql_lean_revert_flag'] = array(
      '#type' => 'checkbox',
      '#title' => 'Lean process.(Even though, there is a reverse process, that is not guarrentee 100% reverse to the origin data.)',
      '#default_value' => FALSE,
      '#weight' => 11,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Lean Alter'),
      '#submit' => array('field_sql_lean_batch_alter_form_lean_submit'),
      '#weight' => 22,
    );
  }
  $form['description1'] = array(
    '#markup' => t('Save the URL of the batch processing page next. If the process was interrupted and stopped in the middle, use it to continue the job.'),
    '#weight' => 7,
  );
  return $form;
}

/**
 * Form submit handler function.
 */
function field_sql_lean_batch_alter_form_lean_submit($form, &$form_state) {
  $reverse = $form_state['values']['field_sql_lean_revert_flag'];
  if (!$reverse) {
    return;
  }
  $batch = array();
  $batch['finished'] = 'field_sql_lean_batch_finish';

  $fields = field_info_fields();

  // Put all heavy database changes in batch process.
  foreach ($fields as $name => $field) {
    if (!db_field_exists('field_revision_' . $name, 'entity')) {
      $batch['operations'][] = array(
        'db_add_field',
        array(
          'field_revision_' . $name,
          'entity',
          array('type' => 'int', 'unsigned' => TRUE),
        ),
      );
    }
    $batch['operations'][] = array(
      'db_query',
      array('update {field_revision_' . $name . '} f set f.entity = (select eid from {fsl_entity} fsle where fsle.entity = f.entity_type);'),
    );

    $batch['operations'][] = array('db_query', array('alter table {field_revision_' . $name . '} modify column entity int unsigned not null'));

    $batch['operations'][] = array(
      'db_drop_field',
      array('field_revision_' . $name, 'entity_type'),
    );

    $batch['operations'][] = array(
      'db_drop_field',
      array('field_revision_' . $name, 'bundle'),
    );

    $batch['operations'][] = array('db_drop_table', array('field_data_' . $name));

    $batch['operations'][] = array(
      'db_drop_field',
      array('field_revision_' . $name, 'deleted'),
    );

    $batch['operations'][] = array('db_drop_primary_key', array('field_revision_' . $name));

    $batch['operations'][] = array(
      'db_add_primary_key',
      array(
        'field_revision_' . $name,
        array(
          'entity_id',
          'revision_id',
          'delta',
          'language',
          'entity',
        ),
      ),
    );

  }

  $batch['operations'][] = array(
    'db_query',
    array('update field_config set storage_module = \'field_sql_lean\', data = REPLACE(data, \'s:17:"field_sql_storage";\', \'s:14:"field_sql_lean";\');'),
  );

  if (db_table_exists('cache')) {
    $batch['operations'][] = array('db_query', array('delete from {cache};'));
  }
  if (db_table_exists('cache_admin_menu')) {
    $batch['operations'][] = array('db_query', array('delete from {cache_admin_menu};'));
  }
  if (db_table_exists('cache_block')) {
    $batch['operations'][] = array('db_query', array('delete from {cache_block};'));
  }
  if (db_table_exists('cache_bootstrap')) {
    $batch['operations'][] = array('db_query', array('delete from {cache_bootstrap};'));
  }
  if (db_table_exists('cache_views')) {
    $batch['operations'][] = array('db_query', array('delete from {cache_views};'));
  }
  if (db_table_exists('cache_path')) {
    $batch['operations'][] = array('db_query', array('delete from {cache_path};'));
  }
  if (db_table_exists('cache_update')) {
    $batch['operations'][] = array('db_query', array('delete from {cache_update};'));
  }
  if (db_table_exists('cache_page')) {
    $batch['operations'][] = array('db_query', array('delete from {cache_page};'));
  }
  if (db_table_exists('cache_rules')) {
    $batch['operations'][] = array('db_query', array('delete from {cache_rules};'));
  }
  if (db_table_exists('cache_menu')) {
    $batch['operations'][] = array('db_query', array('delete from {cache_menu};'));
  }
  if (db_table_exists('cache_image')) {
    $batch['operations'][] = array('db_query', array('delete from {cache_image};'));
  }
  if (db_table_exists('cache_libraries')) {
    $batch['operations'][] = array('db_query', array('delete from {cache_libraries};'));
  }
  if (db_table_exists('cache_field')) {
    $batch['operations'][] = array('db_query', array('delete from {cache_field};'));
  }
  if (db_table_exists('cache_filter')) {
    $batch['operations'][] = array('db_query', array('delete from {cache_filter};'));
  }
  if (db_table_exists('cache_form')) {
    $batch['operations'][] = array('db_query', array('delete from {cache_form};'));
  }

  $batch['operations'][] = array(
    'db_query',
    array("UPDATE {system} SET weight = 10 WHERE name = 'field_sql_lean'"),
  );

  if (!empty($batch['operations'])) {
    batch_set($batch);
  }
}

/**
 * Form submit handler function to reverse.
 */
function field_sql_lean_batch_alter_form_reverse_submit($form, &$form_state) {
  $reverse = $form_state['values']['field_sql_lean_revert_flag'];
  if (!$reverse) {
    return;
  }
  $batch = array();
  $batch['finished'] = 'field_sql_lean_batch_finish_reverse';
  $fields = field_info_fields();

  // Put all heavy database changes in batch process.
  foreach ($fields as $name => $field) {
    if (!db_field_exists('field_revision_' . $name, 'deleted')) {
      $batch['operations'][] = array(
        'db_add_field',
        array(
          'field_revision_' . $name,
          'entity_type',
          array(
            'type' => 'varchar',
            'length' => 128,
            'not null' => TRUE,
            'default' => '',
            'description' => 'The entity type this data is attached to',
          ),
        ),
      );
      $batch['operations'][] = array(
        'db_add_field',
        array(
          'field_revision_' . $name,
          'bundle',
          array(
            'type' => 'varchar',
            'length' => 128,
            'not null' => FALSE,
            'default' => '',
            'description' => 'The field instance bundle to which this row belongs, used when deleting a field instance',
          ),
        ),
      );
      $batch['operations'][] = array(
        'db_add_field',
        array(
          'field_revision_' . $name,
          'deleted',
          array(
            'type' => 'int',
            'size' => 'tiny',
            'not null' => TRUE,
            'default' => 0,
            'description' => 'A boolean indicating whether this data item has been deleted',
          ),
        ),
      );
    }
    $batch['operations'][] = array(
      'db_query',
      array('update {field_revision_' . $name . '} f set f.entity_type = (select entity from {fsl_entity} fsle where fsle.eid = f.entity);'),
    );

    $info = entity_get_info();
    $eid = field_sql_lean_entity_id();

    foreach ($info as $type => $entity_info) {
      if (isset($entity_info['bundle keys']) && $type !== 'taxonomy_term') {
        $bundle = $entity_info['bundle keys']['bundle'];
      }
      else {
        continue;
      }
      $entity_table = $entity_info['base table'];
      $entity_id = $entity_info['entity keys']['id'];
      $batch['operations'][] = array(
        'db_query',
        array('update {field_revision_' . $name . '} f set f.bundle = (select ' . $bundle . ' from {' . $entity_table . '} base where base.' . $entity_id . ' = f.entity_id) where f.entity = ' . $eid[$type] . ';'),
      );
    }

    $batch['operations'][] = array(
      'db_drop_field',
      array('field_revision_' . $name, 'entity'),
    );

    $batch['operations'][] = array('db_drop_primary_key', array('field_revision_' . $name));

    $batch['operations'][] = array(
      'db_add_primary_key',
      array(
        'field_revision_' . $name,
        array(
          'entity_type',
          'deleted',
          'entity_id',
          'revision_id',
          'delta',
          'language',
        ),
      ),
    );
    $schema = _field_sql_storage_schema($field);
    $tablename = _field_sql_storage_tablename($field);
    $table = $schema[$tablename];
    $batch['operations'][] = array('db_create_table', array($tablename, $table));

    $fields_names = array_keys($table['fields']);
    $fields_str = implode(', ', $fields_names);
    $fields_str1 = implode(', f.', $fields_names);
    $fields_str1 = 'f.' . $fields_str1;
    // Comment fields are ignored for now.
    foreach ($info as $type => $entity_info) {
      if (isset($entity_info['bundle keys'])) {
        $bundle = $entity_info['bundle keys']['bundle'];
      }
      else {
        continue;
      }
      $entity_table = $entity_info['base table'];
      $entity_id = $entity_info['entity keys']['id'];
      $entity_vid = $entity_info['entity keys']['revision'];
      if (!empty($entity_vid)) {
        $batch['operations'][] = array(
          'db_query',
          array('insert into {field_data_' . $name . '} (' . $fields_str . ') select ' . $fields_str1 . ' from {field_revision_' . $name . '} f join {' . $entity_table . '} n on n.' . $entity_vid . ' =f.revision_id where n.' . $entity_id . ' = f.entity_id and f.entity_type = \'' . $type . '\''),
        );
      }
      else {
        $batch['operations'][] = array(
          'db_query',
          array('insert into {field_data_' . $name . '} (' . $fields_str . ') select ' . $fields_str1 . ' from {field_revision_' . $name . '} f join {' . $entity_table . '} n on n.' . $entity_id . ' = f.entity_id where f.entity_type = \'' . $type . '\''),
        );

      }
    }

  }

  $batch['operations'][] = array(
    'db_query',
    array('update field_config set storage_module = \'field_sql_storage\', data = REPLACE(data, \'s:14:"field_sql_lean";\', \'s:17:"field_sql_storage";\');'),
  );

  if (db_table_exists('cache')) {
    $batch['operations'][] = array('db_query', array('delete from {cache};'));
  }
  if (db_table_exists('cache_admin_menu')) {
    $batch['operations'][] = array('db_query', array('delete from {cache_admin_menu};'));
  }
  if (db_table_exists('cache_block')) {
    $batch['operations'][] = array('db_query', array('delete from {cache_block};'));
  }
  if (db_table_exists('cache_bootstrap')) {
    $batch['operations'][] = array('db_query', array('delete from {cache_bootstrap};'));
  }
  if (db_table_exists('cache_views')) {
    $batch['operations'][] = array('db_query', array('delete from {cache_views};'));
  }
  if (db_table_exists('cache_path')) {
    $batch['operations'][] = array('db_query', array('delete from {cache_path};'));
  }
  if (db_table_exists('cache_update')) {
    $batch['operations'][] = array('db_query', array('delete from {cache_update};'));
  }
  if (db_table_exists('cache_page')) {
    $batch['operations'][] = array('db_query', array('delete from {cache_page};'));
  }
  if (db_table_exists('cache_rules')) {
    $batch['operations'][] = array('db_query', array('delete from {cache_rules};'));
  }
  if (db_table_exists('cache_menu')) {
    $batch['operations'][] = array('db_query', array('delete from {cache_menu};'));
  }
  if (db_table_exists('cache_image')) {
    $batch['operations'][] = array('db_query', array('delete from {cache_image};'));
  }
  if (db_table_exists('cache_libraries')) {
    $batch['operations'][] = array('db_query', array('delete from {cache_libraries};'));
  }
  if (db_table_exists('cache_field')) {
    $batch['operations'][] = array('db_query', array('delete from {cache_field};'));
  }
  if (db_table_exists('cache_filter')) {
    $batch['operations'][] = array('db_query', array('delete from {cache_filter};'));
  }
  if (db_table_exists('cache_form')) {
    $batch['operations'][] = array('db_query', array('delete from {cache_form};'));
  }

  $batch['operations'][] = array(
    'db_query',
    array("UPDATE {system} SET weight = 0 WHERE name = 'field_sql_lean'"),
  );

  if (!empty($batch['operations'])) {
    batch_set($batch);
  }
}
